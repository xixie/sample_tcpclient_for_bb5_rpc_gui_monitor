#include "tcpclient.h"
#include "ui_tcpclient.h"

TCPClient::TCPClient(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TCPClient)
{
    ui->setupUi(this);

    tcpClient = new QTcpSocket(this);
    tcpClient->abort();
    connect(tcpClient, SIGNAL(readyRead()), this, SLOT(ReadData()));
    connect(tcpClient, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(ReadError(QAbstractSocket::SocketError)));
}

TCPClient::~TCPClient()
{
    delete ui;
}

void TCPClient::on_B_Connect_clicked()
{
    if(ui->B_Connect->text() == "Connect")
    {
        tcpClient->connectToHost(ui->LE_SeverIP->text(), ui->LE_SeverPort->text().toInt());
        if (tcpClient->waitForConnected(1000))
        {
            ui->B_Connect->setText("Disconnect");
        }
    }
    else
    {
        tcpClient->disconnectFromHost();
        if (tcpClient->state() == QAbstractSocket::UnconnectedState || tcpClient->waitForDisconnected(1000))
        {
            qDebug()<<"Not connected";
            ui->B_Connect->setText("Connect");
        }
    }
}

void TCPClient::ReadData()
{
    QByteArray buffer = tcpClient->readAll();
    qDebug() << "buffer size = " << buffer.size();
    if(buffer.size() > 1000)
    {
        trackInfo RecPack;
        memcpy(&RecPack, buffer.data(), sizeof(RecPack));
        QString temp;
        for(int i = 0; i < RecPack.EventsContained; i++)
        {
            if(RecPack.is_track[i] == false)
                continue;
            std::stringstream sentence;
            temp.clear();
            sentence.clear();
            sentence << "Track of Event " << RecPack.EventNo[i] << "\n";
            sentence << "X = " << RecPack.K1[i] << " * Z + " << RecPack.C1[i] << "; Y = " << RecPack.K2[i] << " * Z + " << RecPack.C2[i] << "\n\n";
            temp.append(sentence.str().c_str());
            ui->TB_ReceiverView->append(temp);
        }
        //qDebug() << RecPack.EventNo[0];
    }
    else if(!buffer.isEmpty())
    {
        ui->TB_ReceiverView->append(buffer);
    }
}

void TCPClient::ReadError(QAbstractSocket::SocketError)
{
    tcpClient->disconnectFromHost();
    ui->B_Connect->setText("Connect");
    QMessageBox msgBox;
    msgBox.setText(tr("Failded to connect sever because %1").arg(tcpClient->errorString()));
    msgBox.exec();
}


