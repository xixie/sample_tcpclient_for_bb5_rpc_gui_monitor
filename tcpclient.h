#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QHostAddress>
#include <QMessageBox>
#include <sstream>

const int Event_in_One_Pack = 1000;
typedef struct packedData
{
    int EventsContained;
    int EventNo[Event_in_One_Pack];
    bool is_track[Event_in_One_Pack];
    float K1[Event_in_One_Pack];
    float K2[Event_in_One_Pack];
    float C1[Event_in_One_Pack];
    float C2[Event_in_One_Pack];
} trackInfo;
namespace Ui {
class TCPClient;
}

class TCPClient : public QMainWindow
{
    Q_OBJECT

public:
    explicit TCPClient(QWidget *parent = nullptr);
    ~TCPClient();

private:
    Ui::TCPClient *ui;
    QTcpSocket *tcpClient;

private slots:
    void ReadData();
    void ReadError(QAbstractSocket::SocketError);
    void on_B_Connect_clicked();
};

#endif // TCPCLIENT_H
